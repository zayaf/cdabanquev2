package fr.afpa.controles;



import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.entite.Compte;

public class Controls {
	
	private Controls(){
		
	}
	
	/**
	 * Controle du numero de compte bancaire a 11 chiffres
	 * @param numCompteBancaire
	 * @return vrai si le regex correspond au numero bancaire
	 */
	public static boolean controlNumCompteBancaire(String numCompteBancaire) {
		String regex = "\\d{11}"; 
		if(numCompteBancaire.matches(regex)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Controle de l'identifiant du client 2 lettre en MAJ et 6 chiffres
	 * @param numIdClient
	 * @return vrai si le regex correspond a l'identifiant du client
	 */
	public static boolean controlNumIdClient(String numIdClient) {
		String regex = "[A-Z]{2}\\d{6}";
		if(numIdClient.matches(regex)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Controle code agence 3 chiffres
	 * @param codeAgence
	 * @return vrai si le regex correspond au code agence
	 */
	public static boolean controlCodeAgence(String codeAgence) {
		String regex = "\\d{3}";
		if(codeAgence.matches(regex)) {				
			return true;
		}
		return false;	
	}
	
	/**
	 * Controle du login client a 10 chiffres
	 * @param loginClient
	 * @return vrai si le regex correspond au login du client
	 */
	public static boolean controlLoginClient(String loginClient) {
		String regex = "\\d{10}";
		if(loginClient.matches(regex)) {
			return true;
		}	
		return false;	
	}
	
	/**
	 * Controle du login conseiller CO et 4 chiffres
	 * @param loginConseiller
	 * @return vrai si le regex correspond au login du conseiller
	 */
	public static boolean controlLoginConseiller(String loginConseiller) {
		String regex = "CO\\d{4}";
		if(loginConseiller.matches(regex)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Controle du login admin ADM et 2 chiffres
	 * @param loginAdmin
	 * @return vrai si le regex correspond au login de l'admin
	 */
	public static boolean controlLoginAdmin(String loginAdmin) {
		String regex = "ADM\\d{2}";
		if(loginAdmin.matches(regex)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Controle de l'email d'une personne
	 * @param email
	 * @return vrai si le regex correspond a un email valide
	 */
	public static boolean controlEmail(String email) {
		String regex =  "\\w[\\w\\-]*(\\.[\\w\\-]+)*@[a-zA-Z]{3,13}\\.[a-zA-Z]{2,3}";
		if(email.matches(regex)) {
			return true;
		}
		else {
			System.out.println("Entrez une adresse mail valide :");
			return false;
		}
	}
	
	/**
	 * Controle de la date
	 * @param date
	 * @return faux si la date est pas valide
	 */
	public static boolean checkDate(String date) {

		// boucle tant que la date n'est pas correct, demander la date.	
		String[] donneesDate = date.split("/");
		try {
			LocalDate date1 = LocalDate.parse(donneesDate[2] + "-" + donneesDate[1] + "-" + donneesDate[0]);
			// si la date est correcte, boolean a true et break pour sortir de la boucle
		} catch (Exception e) {
			System.out.println("La date n'est pas valide.");
			return false;
		}		
		return true;
	}
	
	/**
	 * Controle le solde d'un compte selon le montant
	 * @param compte
	 * @param montant
	 * @return faux si le decouvert n'est pas autorisé sinon vrai
	 */
	public static boolean controleSoldeValid(Compte compte,double montant) {
		if(compte.getSoldeCompte() - montant < 0 && compte.isDecouvertCompte()==false) {
			System.out.println("Decouvert non autorisee, vous pouvez pas effectuer un virement ou un retrait");
			return false;
		}
		return true;
	}
}