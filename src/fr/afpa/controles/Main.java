package fr.afpa.controles;

import java.util.Scanner;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Bank;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.CompteLivretA;
import fr.afpa.entite.ComptePEL;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.BankServices;
import fr.afpa.services.CompteService;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Bank banque=new Bank();
Agence agence=new Agence("001", "X", "Y");
Conseiller conseiller=new Conseiller("benjira", "mohamed", "20/03/1982", "email@email.com", "00000001", "0000");
Client client = new Client("ben", "Hakim", "29/03/1979", "123@aol.fr", "123456789", "login", "password");

conseiller.getClients().add(client);
agence.getListConseiller().add(conseiller);
banque.getListAgence().add(agence);


CompteService compte=new CompteService();

compte.creationCompte(banque);
compte.creationCompte(banque);

Client c=BankServices.rechercheClient("123456789", banque);
for (Compte cpt : c.getListeCompteClient()) {
	System.out.println(c);
}
}
}