package fr.afpa.entite;

import java.time.LocalDate;
import java.util.Arrays;

public class Admin extends Conseiller{
	private String loginAdmin;
	private String passwordAdmin;
	
	public Admin() {
		
	}
	
	public Admin(String nom, String prenom, String dateNaissance, String email,String loginAdmin, String passwordAdmin) {
		super(nom, prenom, dateNaissance, email, loginAdmin, passwordAdmin);
		// TODO Auto-generated constructor stub
		this.loginAdmin = loginAdmin;
		this.passwordAdmin = passwordAdmin;
	}

	public String getLoginAdmin() {
		return loginAdmin;
	}
	public void setLoginAdmin(String loginAdmin) {
		this.loginAdmin = loginAdmin;
	}

	public String getPasswordAdmin() {
		return passwordAdmin;
	}

	public void setPasswordAdmin(String passwordAdmin) {
		this.passwordAdmin = passwordAdmin;
	}

	@Override
	public String toString() {
		return "Admin [loginAdmin=" + loginAdmin + ", getLoginConseiller()=" + getLoginConseiller()
				+ ", getPassWordConseiller()=" + getPassWordConseiller() + ", getClients()=" + getClients()
				+ ", toString()=" + super.toString() + ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom()
				+ ", getDateNaissance()=" + getDateNaissance() + ", getEmail()=" + getEmail() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
	
}