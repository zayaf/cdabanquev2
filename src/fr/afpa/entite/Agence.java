package fr.afpa.entite;

import java.util.ArrayList;

public class Agence {
	// Attribut(s)
	private String codeAgence;
	private String nomAgence;
	private String adresseAgence;
	private ArrayList<Conseiller> listConseiller;
	
	public Agence(String codeAgence, String nomAgence, String adresseAgence) {
		super();
		this.codeAgence = codeAgence;
		this.nomAgence = nomAgence;
		this.adresseAgence = adresseAgence;
		this.listConseiller = new ArrayList<Conseiller>();
		
	}

	public String getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(String codeAgence) {
		this.codeAgence = codeAgence;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}

	public String getAdresseAgence() {
		return adresseAgence;
	}

	public void setAdresseAgence(String adresseAgence) {
		this.adresseAgence = adresseAgence;
		
	}

	public ArrayList<Conseiller> getListConseiller() {
		return listConseiller;
	}

	public void setListConseiller(ArrayList<Conseiller> listConseiller) {
		this.listConseiller = listConseiller;
	}

	@Override
	public String toString() {
		return "Agence [codeAgence=" + codeAgence + ", nomAgence=" + nomAgence + ", adresseAgence=" + adresseAgence
				+ "]";
	}
	
	
}
	



	