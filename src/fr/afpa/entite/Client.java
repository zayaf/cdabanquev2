package fr.afpa.entite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import fr.afpa.controles.Controls;

public class Client extends Personne{
		
		// Attribut(s)
		private String id;
		// creating an empty LinkedList 
		private String loginClient;
		private String password;
		private boolean isActif;
		private ArrayList<Compte> listeCompteClient;
		
		
		public String getId() {
			return id;
		}



		public void setId(String id) {
			this.id = id;
		}

		public String getLoginClient() {
			return loginClient;
		}



		public void setLoginClient(String loginClient) {
			this.loginClient = loginClient;
		}



		public String getPassword() {
			return password;
		}



		public void setPassword(String password) {
			this.password = password;
		}



		public  ArrayList<Compte> getListeCompteClient() {

			return listeCompteClient;
		}



		public void setListeCompteClient(ArrayList<Compte> listeCompteClient) {
			this.listeCompteClient = listeCompteClient;
		}



		public boolean isActif() {
			return isActif;
		}



		public void setActif(boolean isActif) {
			this.isActif = isActif;
		}



		public Client(String nom, String prenom, String dateNaissance, String email, String id, String login, String password) {
			super(nom, prenom, dateNaissance, email);
			this.id = id;
			this.loginClient = login;
			this.password = password;
			this.listeCompteClient = new ArrayList<Compte>();
			this.isActif = true;
		}

		

		@Override
		public String toString() {
			return "Client [id=" + id + ", listeCompteClient=" + listeCompteClient + ", getNom()=" + getNom()
					+ ", getPrenom()=" + getPrenom() + ", getDateNaissance()=" + getDateNaissance() + ", getEmail()="
					+ getEmail() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
					+ hashCode() + "]";
		}


}
