package fr.afpa.entite;

import java.util.ArrayList;

public class Bank {
private ArrayList<Agence> listAgence;
private Admin admin;

public Bank() {
	super();
	this.listAgence = new ArrayList<Agence>();
}

public ArrayList<Agence> getListAgence() {
	return listAgence;
}

public void setListAgence(ArrayList<Agence> listAgence) {
	this.listAgence = listAgence;
}

public Admin getAdmin() {
	return this.admin;
}

public void setAdmin(Admin admin) {
	this.admin = admin;
}

}

