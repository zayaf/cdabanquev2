package fr.afpa.entite;
import java.time.LocalDate;
import java.util.LinkedList;

public class CompteLivretA extends Compte {
	
	Double FraisLivretA;
	CompteCourant fraisCompteCourant;
	
		

	

	public CompteLivretA() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CompteLivretA(String numeroCompte, String codeAgenceCompte, double soldeCompte, boolean decouvertCompte) {
		super(numeroCompte, codeAgenceCompte, soldeCompte, decouvertCompte);
		// TODO Auto-generated constructor stub
	}


	public Double getFraisLivretA() {
		return FraisLivretA;
	}


	public void setFraisLivretA(Double fraisLivretA) {
		FraisLivretA = fraisLivretA;
	}


	public CompteCourant getFraisCompteCourant() {
		return fraisCompteCourant;
	}


	/*public void creationCompteLivretA () {
		Compte CompteLivretA=new CompteLivretA();
		if(Client.getListeCompteClient()!= null) {
			Client.getListeCompteClient().add(CompteLivretA);	
		}
	 }*/

	@Override
	public String toString() {
		return "CompteLivretA [FraisLivretA=" + FraisLivretA + ", fraisCompteCourant=" + fraisCompteCourant
				+ ", getFraisLivretA()=" + getFraisLivretA() + ", getFraisCompteCourant()=" + getFraisCompteCourant()
				+ ", getNumeroCompte()=" + getNumeroCompte() + ", getCodeAgenceCompte()=" + getCodeAgenceCompte()
				+ ", toString()=" + super.toString() + ", getSoldeCompte()=" + getSoldeCompte()
				+ ", isDecouvertCompte()=" + isDecouvertCompte() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}


	public void setFraisCompteLivretA(CompteCourant fraisCompteCourant) {
		this.fraisCompteCourant = fraisCompteCourant;


	}
}
	







