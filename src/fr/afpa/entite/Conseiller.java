package fr.afpa.entite;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import fr.afpa.controles.Controls;

public class Conseiller extends Personne{

	private String loginConseiller;
	private String passWordConseiller;
	private ArrayList<Client> clients;


	public Conseiller() {
		
	}

	public Conseiller(String nom, String prenom, String dateNaissance, String email, String loginConseiller,
			String passWordConseiller) {
		super(nom, prenom, dateNaissance, email);
		this.loginConseiller = loginConseiller;
		this.passWordConseiller = passWordConseiller;
		this.clients = new ArrayList<Client>();
	}
	
	

	public String getLoginConseiller() {
		return loginConseiller;
	}
	public void setLoginConseiller(String loginConseiller) {
		this.loginConseiller = loginConseiller;
	}
	public String getPassWordConseiller() {
		return passWordConseiller;
	}
	public void setPassWordConseiller(String passWordConseiller) {
		this.passWordConseiller = passWordConseiller;
	}
	public ArrayList<Client> getClients() {
		return clients;
	}
	public void setClients(ArrayList<Client> clients) {
		this.clients = clients;
	}





	@Override
	public String toString() {
		return "Conseiller [loginConseiller=" + loginConseiller + ", passWordConseiller=" + passWordConseiller
				+ ", clients=" + clients + ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom()
				+ ", getDateNaissance()=" + getDateNaissance() + ", getEmail()=" + getEmail() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";

	}

}


