package fr.afpa.entite;

import java.util.ArrayList;

public class Compte {
	private String numeroCompte;
	private String codeAgenceCompte;
	protected double soldeCompte;
	private boolean decouvertCompte;
	
	
	public Compte() {
		
	}
	public Compte(String numeroCompte, String codeAgenceCompte, double soldeCompte, boolean decouvertCompte) {
		super();
		this.numeroCompte = numeroCompte;
		this.codeAgenceCompte = codeAgenceCompte;
		this.soldeCompte = soldeCompte;
		this.decouvertCompte = decouvertCompte;
	}
	public String getNumeroCompte() {
		return numeroCompte;
	}
	public void setNumeroCompte(String numeroCompte) {
		this.numeroCompte = numeroCompte;
	}
	public String getCodeAgenceCompte() {
		return codeAgenceCompte;
	}
	public void setCodeAgenceCompte(String codeAgenceCompte) {
		this.codeAgenceCompte = codeAgenceCompte;
	}
	@Override
	public String toString() {
		return "Compte [numeroCompte=" + numeroCompte + ", codeAgenceCompte=" + codeAgenceCompte + ", soldeCompte="
				+ soldeCompte + ", decouvertCompte=" + decouvertCompte + ", ListeCompte=" 
				+ ", getNumeroCompte()=" + getNumeroCompte() + ", getCodeAgenceCompte()=" + getCodeAgenceCompte()
				+ ", getSoldeCompte()=" + getSoldeCompte() + ", isDecouvertCompte()=" + isDecouvertCompte()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	public double getSoldeCompte() {
		return soldeCompte;
	}
	public void setSoldeCompte(double soldeCompte) {
		this.soldeCompte = soldeCompte;
	}
	public boolean isDecouvertCompte() {
		return decouvertCompte;
	}
	public void setDecouvertCompte(boolean decouvertCompte) {
		this.decouvertCompte = decouvertCompte;
	}
	
	
}
