package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.Controls;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Bank;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.BankServices;
import fr.afpa.services.ClientServices;
import fr.afpa.services.CompteService;
import fr.afpa.services.ConseillerServices;

public class ihmSearch {
	
	/**
	 * Affiche les infos d'un client depuis l'admin
	 * @param banque
	 */
	public static void getInfosClient(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'identifiant du client pour lequel vous voulez voir les infos :");
		String idClient = in.nextLine();
		Client client = BankServices.rechercheClient(idClient, banque);
		if(client != null && client.isActif()) {
			System.out.println("Nom : "+client.getNom());
			System.out.println("Prenom : "+client.getPrenom());
			System.out.println("DateNaissance : "+client.getDateNaissance());
			System.out.println("Email : "+client.getEmail());
			System.out.println("Id : "+client.getId());
			System.out.println("Login : "+client.getLoginClient());
		}
		
	}
	
	/**
	 * Affiche les infos d'un client depuis le client
	 * @param client
	 */
	public static void getInfosClient(Client client) {
		if(client != null && client.isActif()) {
			System.out.println("Nom : "+client.getNom());
			System.out.println("Prenom : "+client.getPrenom());
			System.out.println("DateNaissance : "+client.getDateNaissance());
			System.out.println("Email : "+client.getEmail());
			System.out.println("Id : "+client.getId());
			System.out.println("Login : "+client.getLoginClient());
		}
	}
	
	/**
	 * affiche les infos d'un client depuis le conseiller
	 * @param conseiller
	 */
	public static void getInfosClient(Conseiller conseiller) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'identifiant du client pour lequel vous voulez voir les infos :");
		String idClient = in.nextLine();
		Client client = ConseillerServices.rechercheClient(idClient, conseiller);
		if(client != null && client.isActif()) {
			System.out.println("Nom : "+client.getNom());
			System.out.println("Prenom : "+client.getPrenom());
			System.out.println("DateNaissance : "+client.getDateNaissance());
			System.out.println("Email : "+client.getEmail());
			System.out.println("Id : "+client.getId());
			System.out.println("Login : "+client.getLoginClient());
		}
	}
	
	/**Affiche les infos du compte depuis l'admin
	 * 
	 * @param banque
	 */
	public static void getInfosCompte(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'identifiant du client pour voir ses comptes");
		String idClient = in.nextLine();
		Client client = BankServices.rechercheClient(idClient, banque);
		if(client != null && client.isActif()) {
			for (Compte compte : client.getListeCompteClient()) {
				if(compte != null) {
					System.out.print("Numero de compte : "+compte.getNumeroCompte());
					System.out.print(" Solde : "+compte.getSoldeCompte());
					System.out.print(" "+CompteService.smiley(compte));
				}
			}
		}	
	}
	
	/**
	 * affiche les infos du compte depuis conseiller
	 * @param conseiller
	 */
	public static void getInfosCompte(Conseiller conseiller) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'identifiant du client pour voir ses comptes");
		String idClient = in.nextLine();
		Client client = ConseillerServices.rechercheClient(idClient, conseiller);
		if(client != null && client.isActif()) {
			for (Compte compte : client.getListeCompteClient()) {
				if(compte != null) {
					System.out.print("Numero de compte : "+compte.getNumeroCompte());
					System.out.print(" Solde : "+compte.getSoldeCompte());
					System.out.print(" "+CompteService.smiley(compte));
				}
			}
		}	
	}
	
	/**
	 * affiche les infos du compte depuis le client
	 * @param client
	 */
	public static void getInfosCompte(Client client) {
		if(client != null && client.isActif()) {
			for (Compte compte : client.getListeCompteClient()) {
				if(compte != null) {
					System.out.print("Numero de compte : "+compte.getNumeroCompte());
					System.out.print(" Solde : "+compte.getSoldeCompte());
					System.out.print(" "+CompteService.smiley(compte));
				}
			}
		}	
	}
	
	/**
	 * Alimenter le compte avec la transaction dans le fichier depuis l'admin
	 * @param banque
	 */
	public static void alimenterCompte(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrer le numero de compte a alimenter :");
		String numCompte = in.nextLine();
		System.out.println("Entrer le montant que vous voulez :");
		double montant = in.nextDouble();
		in.nextLine();
		boolean operationReussi = false;
		Compte compte = CompteService.rechercheCompte(numCompte, banque);
		if(compte != null) {
			operationReussi = CompteService.alimenterCompte(compte, montant);
		}
		if(operationReussi) {
			System.out.println("Operation reussi");
		}
		else {
			System.out.println("Operation impossible");
		}
	}
	
	/**
	 * Alimenter le compte avec la transaction dans le fichier depuis le client
	 * @param client
	 */
	public static void alimenterCompte(Client client) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrer le numero de compte a alimenter :");
		String numCompte = in.nextLine();
		System.out.println("Entrer le montant que vous voulez :");
		double montant = in.nextDouble();
		in.nextLine();
		boolean operationReussi = false;
		Compte compte = ClientServices.rechercheCompte(numCompte, client);
		if(compte != null) {
			operationReussi = CompteService.alimenterCompte(compte, montant);
		}
		if(operationReussi) {
			System.out.println("Operation reussi");
		}
		else {
			System.out.println("Operation impossible");
		}
	}
	
	/**
	 * Alimenter le compte avec la transaction dans le fichier depuis le conseiller
	 * @param conseiller
	 */
	public static void alimenterCompte(Conseiller conseiller) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrer le numero de compte a alimenter :");
		String numCompte = in.nextLine();
		System.out.println("Entrer le montant que vous voulez :");
		double montant = in.nextDouble();
		in.nextLine();
		boolean operationReussi = false;
		Compte compte = ConseillerServices.rechercheCompte(numCompte, conseiller);
		if(compte != null) {
			operationReussi = CompteService.alimenterCompte(compte, montant);
		}
		if(operationReussi) {
			System.out.println("Operation reussi");
		}
		else {
			System.out.println("Operation impossible");
		}
	}
	
	/**
	 * Retirer l'argent depuis l'admin
	 * @param banque
	 */
	public static void retraitArgent(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrer le numero de compte a debiter :");
		String numCompte = in.nextLine();
		System.out.println("Entrer le montant que vous voulez :");
		double montant = in.nextDouble();
		in.nextLine();
		boolean operationReussi = false;
		Compte compte = CompteService.rechercheCompte(numCompte, banque);
		if(compte != null) {
			operationReussi = CompteService.retraitArgent(compte, montant);
		}
		if(operationReussi) {
			System.out.println("Operation r�ussi");
		}
		else {
			System.out.println("Operation impossible");
		}
	}
	
	/**
	 * Retirer l'argent depuis le conseiller
	 * @param conseiller
	 */
	public static void retraitArgent(Conseiller conseiller) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrer le numero de compte a debiter :");
		String numCompte = in.nextLine();
		System.out.println("Entrer le montant que vous voulez :");
		double montant = in.nextDouble();
		in.nextLine();
		boolean operationReussi = false;
		Compte compte = ConseillerServices.rechercheCompte(numCompte, conseiller);
		if(compte != null) {
			operationReussi = CompteService.retraitArgent(compte, montant);
		}
		if(operationReussi) {
			System.out.println("Operation reussi");
		}
		else {
			System.out.println("Operation impossible");
		}
	}
	
	/**
	 * Retirer l'argent depuis le client
	 * @param client
	 */
	public static void retraitArgent(Client client) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrer le numero de compte � d�biter :");
		String numCompte = in.nextLine();
		System.out.println("Entrer le montant que vous voulez :");
		double montant = in.nextDouble();
		in.nextLine();
		boolean operationReussi = false;
		Compte compte = ClientServices.rechercheCompte(numCompte, client);
		if(compte != null) {
			operationReussi = CompteService.retraitArgent(compte, montant);
		}
		if(operationReussi) {
			System.out.println("Op�ration r�ussi");
		}
		else {
			System.out.println("Op�ration impossible");
		}
	}
	
	/**
	 * Changer les informations du client si l'operation reussi, on affiche ses informations
	 * @param banque
	 */
	public static void changeInfosClient(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'indentifiant du client pour changer ses infos ");
		String idClient = in.nextLine();
		Client client = BankServices.rechercheClient(idClient, banque);
		boolean operationReussi = false;
		if(client != null) {
			operationReussi = ClientServices.changeInfosClient(client);
		}
		if(operationReussi) {
			System.out.println("Nom : "+client.getNom());
			System.out.println("Prenom : "+client.getPrenom());
			System.out.println("DateNaissance : "+client.getDateNaissance());
			System.out.println("Email : "+client.getEmail());
			System.out.println("Id : "+client.getId());
			System.out.println("Login : "+client.getLoginClient());
		}
		else {
			System.out.println("Op�ration impossible");
		}
	}
	
	/**
	 * Changer les infos du client depuis le conseiller
	 * @param conseiller
	 */
	public static void changeInfosClient(Conseiller conseiller) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'indentifiant du client pour changer ses infos ");
		String idClient = in.nextLine();
		Client client = ConseillerServices.rechercheClient(idClient, conseiller);
		boolean operationReussi = false;
		if(client != null) {
			operationReussi = ClientServices.changeInfosClient(client);
		}
		if(operationReussi) {
			System.out.println("Nom : "+client.getNom());
			System.out.println("Prenom : "+client.getPrenom());
			System.out.println("DateNaissance : "+client.getDateNaissance());
			System.out.println("Email : "+client.getEmail());
			System.out.println("Id : "+client.getId());
			System.out.println("Login : "+client.getLoginClient());
		}
		else {
			System.out.println("Op�ration impossible");
		}
	}
	
	/**
	 * Desactiver le client
	 * @param banque
	 */
	public static void desactiverClient(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'indentifiant du client pour le desactiver : ");
		String idClient = in.nextLine();
		Client client = BankServices.rechercheClient(idClient, banque);
		if(client != null) {
			client.setActif(false);
			System.out.println("Le client a été desactivé");
		}
	}
	
	/**
	 * afficher les operations du compte mais ne fonctionne pas
	 */
	public static void afficherOperationCompte() {
		
	}
	
	
	
	/*public static void changeDomiciClient(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'indentifiant du client pour changer son domiciliation");
		String idClient = in.nextLine();
		String codeAgence;
		do {
			System.out.println("Entrez le code de l'autre agence :");
			codeAgence = in.nextLine();
		}while(Controls.controlCodeAgence(codeAgence));
		
		Agence agence = BankServices.rechercheAgence(codeAgence, banque);	
		Client client = BankServices.rechercheClient(idClient, banque);
		
		if(agence != null && client != null) {
			
		}
	}*/
}
