package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entite.Admin;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Bank;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.AgenceServices;
import fr.afpa.services.BankServices;
import fr.afpa.services.ClientServices;

public class ViewConseiller {
	
	//If CHECK LOGIN ET MDP CONSEILLER OK!!!
		public static String afficherMenuConseiller(Conseiller conseiller) {
			Scanner in = new Scanner(System.in);
			System.out.println("Bonjour "+conseiller.getPrenom()+" "+conseiller.getNom()+"  bienvenu dans votre espace Conseiller BANQUE CDA");
			System.out.println("----------------MENU--------------");	
			System.out.println("1-Consulter vos informations");	
			System.out.println("2-Consulter vos comptes");	
			System.out.println("3-Consulter les operations sur vos comptes");	
			System.out.println("4-Faire un virement");	
			System.out.println("5-Imprimer un releve de compte sur une periode");	
			System.out.println("6-Alimenter votre compte");	
			System.out.println("7-Effectuer un retrait");	
			System.out.println("8-Creer un compte");
			System.out.println("9-Creer un Client");
			System.out.println("10-Changer la domiciliation bancaire d'un client");
			System.out.println("11-Modifier les informations d'un client");
			System.out.println("12-Retour vers le menu login");
			System.out.println("13-Quitter");
			String choix = in.nextLine();
			return choix;
		}
		
		public static void choixMenu(Bank banque, Conseiller conseiller) {
			String choix = "";
			while(!"13".equals(choix)) {
				choix = ViewConseiller.afficherMenuConseiller(conseiller);
				switch(choix) {
				case "1": ihmSearch.getInfosClient(conseiller);  // consulter les informations du client
					break;
				case "2": ihmSearch.getInfosCompte(conseiller); // consulter les compte 
					break;
				case "3": System.out.println("Non fonctionnelle pour le moment");// consulter les operations sur les comptes // mercredi
					break;
				case "4": OperationBancaireView.faireVirement(conseiller); // faire un virement
					break;
				case "5": System.out.println("Non fonctionnelle pour le moment");// imprimer un releve de compte Mercredi
					break;
				case "6": ihmSearch.alimenterCompte(conseiller);// alimenter un compte 
					break;
				case "7": ihmSearch.retraitArgent(conseiller);// effectuer un retrait 
					break;
				case "8": System.out.println("Non fonctionnelle pour le moment");// creer un compte HA
					break;
				case "9": ClientServices.createClient(banque);	// creer un client			
					break;
				case "10": System.out.println("Non fonctionnelle pour le moment");// change domici client // pas reussi
					break;
				case "11": ihmSearch.changeInfosClient(conseiller);// modif infos client
					break;
				case "12": AffichageLogin.afficheMenuLogin(banque); // retour vers le menu login
					break;
				case "13": System.out.println("Byebye"); // quitter l'application
					break;
				default: System.out.println("Entrez un choix valide");break;
				}
				
			}
		}
}