package fr.afpa.ihm;
import fr.afpa.entite.Admin;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Bank;
import fr.afpa.services.AgenceServices;
import fr.afpa.services.BankServices;
import fr.afpa.services.ConseillerServices;

public class MainBanque {

	public static void main(String[] args) {
		//Scanner in = new Scanner(System.in);
		// TODO Auto-generated method stub
		Bank banque = new Bank();
		Admin admin = BankServices.createAdmin();
		banque.setAdmin(admin);	
		ViewAdmin.choixMenu(banque, admin);
		
		
	}

}
