package fr.afpa.ihm;


import java.util.Scanner;

import fr.afpa.entite.Admin;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Bank;
import fr.afpa.entite.Client;
import fr.afpa.services.AgenceServices;
import fr.afpa.services.BankServices;
import fr.afpa.services.ClientServices;
public class ViewClient {
	
	//If CHECK LOGIN ET MDP CLIENT OK!!!
	public static String afficherMenuClient(Client client) {
		Scanner in = new Scanner(System.in);
		System.out.println("Bonjour "+client.getPrenom()+" "+client.getNom()+"  bienvenu dans votre espace client BANQUE CDA");
		System.out.println("----------------MENU--------------");	
		System.out.println("1-Consulter vos informations");	
		System.out.println("2-Consulter vos comptes");	
		System.out.println("3-Consulter les op�rations sur vos comptes");	
		System.out.println("4-Faire un virement");	
		System.out.println("5-Imprimer un relev� de compte sur une p�riode");	
		System.out.println("6-Alimenter votre compte");	
		System.out.println("7-Effectuer un retrait");	
		System.out.println("8-Retour vers le menu login");
		System.out.println("9-Quitter");		
		String choix = in.nextLine();
		return choix;
		}	
	
	/**
	 * les choix du menu client
	 * @param banque
	 * @param client
	 * @param admin
	 */
	public static void choixMenu(Bank banque, Client client) {
		String choix = "";
		while(!"9".equals(choix)) {
			choix = ViewClient.afficherMenuClient(client);
			switch(choix) {
			case "1": ihmSearch.getInfosClient(client); // consulter ses informations
				break;
			case "2": ihmSearch.getInfosCompte(client); // consulter ses comptes 
				break;
			case "3": System.out.println("Non fonctionnelle pour le moment");// consulter les operations sur les comptes // mercredi
				break;
			case "4": OperationBancaireView.faireVirement(client); // faire un virement
				break;
			case "5": System.out.println("Non fonctionnelle pour le moment");// imprimer un releve de compte Mercredi
				break;
			case "6": ihmSearch.alimenterCompte(client);// alimenter son compte 
				break;
			case "7": ihmSearch.retraitArgent(client);// effectuer un retrait
				break;
			case "8": AffichageLogin.afficheMenuLogin(banque);// retour vers le menu login
				break;
			case "9": System.out.println("Byebye"); // quitter l'application
				break;
			default: System.out.println("Entrez un choix valide");break;
			}			
		}
	}
}
		

	


