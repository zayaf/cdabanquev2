package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entite.Bank;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.ClientServices;
import fr.afpa.services.CompteService;
import fr.afpa.services.ConseillerServices;

public class OperationBancaireView {
	
	/**
	 * effectuer un virement depuis l'admin
	 * @param banque
	 */
	public static void faireVirement(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez un compte à débiter pour le virement :");
		String numCompteAD = in.nextLine();
		System.out.println("Entrez le ocmpte à créditer pour le virement :");
		String numCompteAC = in.nextLine();
		System.out.println("Entrez le montant que vous voulez verser :");
		double montant = in.nextDouble();
		in.nextLine();
		Compte compteAD = CompteService.rechercheCompte(numCompteAD, banque);
		Compte compteAC = CompteService.rechercheCompte(numCompteAC, banque);
		CompteService.virement(compteAD, compteAC, montant);	
	}
	
	/**
	 * effectuer un virement depuis le client
	 * @param client
	 */
	public static void faireVirement(Client client) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez un compte à débiter pour le virement :");
		String numCompteAD = in.nextLine();
		System.out.println("Entrez le ocmpte à créditer pour le virement :");
		String numCompteAC = in.nextLine();
		System.out.println("Entrez le montant que vous voulez verser :");
		double montant = in.nextDouble();
		in.nextLine();
		Compte compteAD = ClientServices.rechercheCompte(numCompteAD, client );
		Compte compteAC = ClientServices.rechercheCompte(numCompteAC, client);
		CompteService.virement(compteAD, compteAC, montant);	
	}
	
	/**
	 * effectuer un virement depuis le conseiller
	 * @param conseiller
	 */
	public static void faireVirement(Conseiller conseiller) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez un compte à débiter pour le virement :");
		String numCompteAD = in.nextLine();
		System.out.println("Entrez le ocmpte à créditer pour le virement :");
		String numCompteAC = in.nextLine();
		System.out.println("Entrez le montant que vous voulez verser :");
		double montant = in.nextDouble();
		in.nextLine();
		Compte compteAD = ConseillerServices.rechercheCompte(numCompteAD, conseiller );
		Compte compteAC = ConseillerServices.rechercheCompte(numCompteAC, conseiller);
		CompteService.virement(compteAD, compteAC, montant);	
	}
}
