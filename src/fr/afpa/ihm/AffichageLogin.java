package fr.afpa.ihm;
import java.util.Scanner;

import fr.afpa.entite.Admin;
import fr.afpa.entite.Bank;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.LoginService;


public class AffichageLogin {
	
	/**
	 * Affiche le menu login pour se connecter
	 * @param banque
	 */
	public static void afficheMenuLogin(Bank banque) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Etes vous un client, un conseiller ou un admin ?");
		System.out.println("1-Client\n2-Conseiller\n3-Admin\n4-Quitter");
		String choix = sc.nextLine();
		while(!"4".equals(choix)) {
			switch(choix) {
			case "1" : Client client = LoginService.loginClient(banque);
			if(client != null) {
				ViewClient.choixMenu(banque, client);;
			}			
			break;
			case "2" : Conseiller conseiller = LoginService.loginConseiller(banque);
			if(conseiller != null) {
				ViewConseiller.choixMenu(banque, conseiller);
			}		
			break;
			case "3" : Admin admin = LoginService.loginAdmin(banque);
				if(admin != null) {
				ViewAdmin.choixMenu(banque,admin);
			}
			break;
			case "4" : System.out.println("Au revoir");
			break;
			default: System.out.println("Erreur option");
			break;	
			}
		}
		
	}
}
