package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entite.Admin;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Bank;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.AgenceServices;
import fr.afpa.services.BankServices;
import fr.afpa.services.ClientServices;
import fr.afpa.services.CompteService;
import fr.afpa.services.ConseillerServices;

public class ViewAdmin {
	
	//If CHECK LOGIN ET MDP ADMIN OK!!!
			public static String afficherMenuAdmin(Admin admin) {
				Scanner in = new Scanner(System.in);
				System.out.println("Bonjour "+admin.getPrenom()+" "+admin.getNom()+"  bienvenu dans votre espace Administrateur BANQUE CDA");
				System.out.println();
				System.out.println("----------------MENU--------------");	
				System.out.println("1-Consulter vos informations");	
				System.out.println("2-Consulter vos comptes");	
				System.out.println("3-Consulter les operations sur vos comptes");	
				System.out.println("4-Faire un virement");	
				System.out.println("5-Imprimer un releve de compte sur une periode");	
				System.out.println("6-Alimenter votre compte");	
				System.out.println("7-Effectuer un retrait");	
				System.out.println("8-Creer un compte");
				System.out.println("9-Creer un Client");
				System.out.println("10-Changer la domiciliation bancaire d'un client");
				System.out.println("11-Modifier les informations d'un client");
				System.out.println("12-Creer une Agence");
				System.out.println("13-Creer un conseiller");
				System.out.println("14-Desactiver un client");
				System.out.println("15-Desactiver un compte");
				System.out.println("16-Retour vers le menu login");
				System.out.println("17-Quitter");
				System.out.println("Entrez votre choix :");
				String choix = in.nextLine();
				return choix;
				
				
			}
			
			public static void choixMenu(Bank banque, Admin admin) {
				String choix = "";
				while(!"17".equals(choix)) {
					choix = ViewAdmin.afficherMenuAdmin(admin);
					switch(choix) {
					case "1": ihmSearch.getInfosClient(banque); // consulter les informations du client
						break;
					case "2": ihmSearch.getInfosCompte(banque); // consulter les comptes
						break;
					case "3": System.out.println("Non fonctionnelle pour le moment");// consulter les operations sur les comptes //
						break;
					case "4": OperationBancaireView.faireVirement(banque); // faire un virement
						break;
					case "5": System.out.println("Non fonctionnelle pour le moment ");// imprimer un releve de compte Mercredi
						break;
					case "6": ihmSearch.alimenterCompte(banque);// alimenter un compte 
						break;
					case "7": ihmSearch.retraitArgent(banque);// effectuer un retrait 
						break;
					case "8": CompteService.creationCompte(banque); // creer un compte pour un client
						break;
					case "9": ClientServices.createClient(banque); // creer un client
						break;
					case "10": // change domici client // pas reussi
						break;
					case "11": ihmSearch.changeInfosClient(banque);// modif infos client
						break;
					case "12": BankServices.createAgence(banque); // creer une agence
						break;
					case "13": 
						Agence agence = BankServices.getAgence(banque); 
						AgenceServices.createConseiller(agence); // creer un conseiller
						break;
					case "14": ihmSearch.desactiverClient(banque); // desactiver un client
						break;
					case "15": System.out.println("Non fonctionnelle pour le moment");
						break;
					case "16": AffichageLogin.afficheMenuLogin(banque);// retour vers le menu login
					case "17": System.out.println("Byebye"); // quitter l'application
						break;
					default: System.out.println("Entrez un choix valide");break;
					}					
				}
			}
}
