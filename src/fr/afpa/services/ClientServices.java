package fr.afpa.services;

import java.util.LinkedList;
import java.util.Scanner;

import fr.afpa.controles.Controls;
import fr.afpa.entite.*;


public class ClientServices {
	
	/**
	 * Consulter information du client
	 * @param client
	 */
	public void consulterInformation(Client client) {
		System.out.println("Nom : "+client.getNom());
		System.out.println("Prenom : "+client.getPrenom());
		System.out.println("Date de naissance : "+client.getDateNaissance());
		System.out.println("Email : "+client.getEmail());
		for (Compte compte : client.getListeCompteClient()) {
			System.out.println("Compte :"+compte.getNumeroCompte());
		}
		
	}
	
	/**
	 * Consulter les informations de ses comptes
	 * @param client
	 */
	public void consulterComptes(Client client) {
		for (Compte compte : client.getListeCompteClient()) {
			if(compte instanceof CompteCourant) {
				System.out.println("Compte courant : "+compte.getNumeroCompte());
				System.out.println("Solde : "+compte.getSoldeCompte());
			}
		}
		
	}

	
	/**
	 * Creer un client
	 * @param banque
	 * @return un client
	 */
	public static Client createClient(Bank banque) {
		
		Conseiller conseiller = AgenceServices.getConseiller(banque);
		if(conseiller != null) {
			Scanner in = new Scanner(System.in);
			System.out.println("Entrer un nom pour le client:");
			String nom = in.nextLine();
			System.out.println("Entrer un prenom  :");
			String prenom = in.nextLine();
			String email;
			String id;
			String login;
			String date;
			do {
				System.out.println("Entrer une date de naissance ");
				date=in.nextLine();
			}
			
			while (!Controls.checkDate(date));
			do {
				System.out.println("Entrez un email :");
				email = in.nextLine();
			}
			while(!Controls.controlEmail(email));
			do {
				System.out.println("Entrez un identifiant (2 lettres en majuscule + 6 chiffres) :");
				id = in.nextLine();
			}
			while(!Controls.controlNumIdClient(id));
			do {
				System.out.println("Entrez un login (Entrez 10 chiffres) :");
				login = in.nextLine();
			}
			while(!Controls.controlLoginClient(login));
			
			System.out.println("Entrez un mot de passe");
			String password = in.nextLine();
			
			Client client = new Client(nom, prenom, date, email, id, login, password);
			conseiller.getClients().add(client);
			Operation.createFichier(id, login, password, "C:\\ENV\\Ressources\\Client.txt");
			return client;	
		}
		System.out.println("Cr�er un conseiller d'abord");
		return null;
		
	}
	
		/**
		 * change les informations du clients !
		 * @param client
		 * @return
		 */
		public static boolean changeInfosClient(Client client) {
			if(client != null) {
				Scanner in = new Scanner(System.in);
				System.out.println("Entrer un nom pour le client:");
				String nom = in.nextLine();
				System.out.println("Entrer un prenom  :");
				String prenom = in.nextLine();
				String email;
				String id;
				String login;
				String date;
				do {
					System.out.println("Entrer une date de naissance ");
					date=in.nextLine();
				}
				
				while (!Controls.checkDate(date));
				do {
					System.out.println("Entrez un email :");
					email = in.nextLine();
				}
				while(!Controls.controlEmail(email));
				do {
					System.out.println("Entrez un identifiant (2 lettres en majuscule + 6 chiffres) :");
					id = in.nextLine();
				}
				while(!Controls.controlNumIdClient(id));
				do {
					System.out.println("Entrez un login (Entrez 10 chiffres) :");
					login = in.nextLine();
				}
				while(!Controls.controlLoginClient(login));
				
				System.out.println("Entrez un mot de passe");
				String password = in.nextLine();
				client.setNom(nom);
				client.setPrenom(prenom);
				client.setDateNaissance(date);
				client.setEmail(email);
				client.setId(id);
				client.setLoginClient(login);
				client.setPassword(password);
				return true;
				
			}
			return false;
		}
		
		
		 /**
		  * Recherche d'un compte depuis le client
		  * @param numCompte
		  * @param client
		  * @return un compte
		  */
			public static Compte rechercheCompte(String numCompte, Client client) { 
				for(Compte compte : client.getListeCompteClient()) {
					if(numCompte != null && numCompte.equals(compte.getNumeroCompte())) {
						return compte;
					}
				}
				return null;
			}
		
		/*public static boolean changeDomiciClient(Agence agence, Client client) {
		
			
		}*/
}
