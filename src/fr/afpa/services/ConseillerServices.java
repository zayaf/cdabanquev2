package fr.afpa.services;

import java.util.LinkedList;
import java.util.Scanner;

import fr.afpa.controles.Controls;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Bank;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;

public class ConseillerServices {

	  
	/**
	 * Création d'un conseiller depuis une agence
	 * @param agence
	 * @return un conseiller
	 */
	public Conseiller createConseiller(Agence agence) {
		if(agence!=null) {
			Scanner in = new Scanner(System.in);
			System.out.println("Entrer un nom pour le conseiller:");
			String nom = in.nextLine();
			System.out.println("Entrer un prenom  :");
			String prenom = in.nextLine();
			String email;
			String login;
			
			String date;
			do {
				System.out.println("Entrer une date de naissance ");
				date=in.nextLine();
			}while (!Controls.checkDate(date));
			
			do {
				System.out.println("Entrez un email :");
				email = in.nextLine();
			}while(!Controls.controlEmail(email));
			
			do {
				System.out.println("Entrez un login (CO et 4 chiffres):");
				login = in.nextLine();
			}while(!Controls.controlLoginConseiller(login));
			
			System.out.println("Entrez un mot de passe");
			String password = in.nextLine();
			Conseiller conseiller = new Conseiller(nom, prenom, date, email, login, password);
			return conseiller;
			}
		System.out.println("Creer une agence d'abord");
		return null;
	}
	
	/**
	 * Recherche de client dans une agence
	 * @param idClient
	 * @param agence
	 * @return un client
	 */
	public static Client rechercheClient(String idClient, Agence agence) {
			for(Conseiller conseiller : agence.getListConseiller()) {
				for(Client client : conseiller.getClients()) {
					if(idClient != null && idClient.equals(client.getId()) && client.isActif());
					return client;
				}
			}			
		return null;
	}
	
	
	/**
	 * Recherche de compte depuis conseiller 
	 * @param numCompte
	 * @param conseiller
	 * @return un compte
	 */
	public static Compte rechercheCompte(String numCompte, Conseiller conseiller) { 
		for(Client client : conseiller.getClients()) {
			for(Compte compte : client.getListeCompteClient()) {
				if(numCompte != null && numCompte.equals(compte.getNumeroCompte()) && client.isActif()) {
					return compte;
				}
			}
		}
		return null;
	}
	
	/**
	 * Recherche de client depuis le conseiller
	 * @param idClient
	 * @param conseiller
	 * @return un client
	 */
	public static Client rechercheClient(String idClient, Conseiller conseiller) {
		for(Client client : conseiller.getClients()) {
			if(idClient != null && idClient.equals(client.getId()) && client.isActif());
				return client;
			}
		return null;
	}

}
