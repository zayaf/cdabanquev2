package fr.afpa.services;
import java.util.Scanner;
import fr.afpa.entite.*;

import fr.afpa.controles.Controls;
import fr.afpa.entite.*;

public class BankServices {

	/**
	 * Creation d'un admin
	 * @return un admin
	 */
	public static Admin createAdmin() {
		Scanner in = new Scanner(System.in);
		String nom;
		String prenom;
		String dateNaissance;
		String email;
		String loginAdmin;
		String passwordAdmin;
		System.out.println("Entrez le nom de votre admin");
		nom = in.nextLine();
		System.out.println("Entrez le prenom de votre admin");
		prenom = in.nextLine();
		do {
			System.out.println("Entrez la date de naissance de votre admin");
			dateNaissance = in.nextLine();
		}while(!Controls.checkDate(dateNaissance));
		
		do{
			System.out.println("Entrez l'email de votre admin");
			email = in.nextLine();
		}while(!Controls.controlEmail(email));
		do {
			System.out.println("Entrez le login de votre admin (ADM et 2 chiffres)");
			loginAdmin = in.nextLine();
		}while(!Controls.controlLoginAdmin(loginAdmin));
		
		System.out.println("Entrez le mot de passe de votre admin");
		passwordAdmin = in.nextLine();	
		Admin admin = new Admin(nom, prenom, dateNaissance, email, loginAdmin, passwordAdmin);
		return admin;
	}
	
	/**
	 * creation de l'agence
	 * @param banque
	 */
	public static void createAgence(Bank banque) {
		Agence agence  = new AgenceServices().createAgence();
		
		if(agence!= null) {
			banque.getListAgence().add(agence);		
			for (Agence agency : banque.getListAgence()) {
				System.out.println("Nom agence " + agency.getNomAgence() + ", Code agence : "+ agency.getCodeAgence() );
			}
		}	
	}
	
	/**
	 * Recherche de l'agence dans banque
	 * @param banque
	 * @return une agence
	 */
	public static Agence getAgence(Bank banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez votre code Agence :");
		String codeAgence = in.nextLine();
		for (Agence agence : banque.getListAgence()) {
			if(codeAgence.equals(agence.getCodeAgence())) {
				return agence;
			}
		}
		return null;
	}
	
	/**
	 * Recherche de client dans la banque
	 * @param idClient identifiant du client
	 * @param banque
	 * @return un client
	 */
	public static Client rechercheClient(String idClient, Bank banque) {
		for (Agence agence : banque.getListAgence()) {
			for(Conseiller conseiller : agence.getListConseiller()) {
				for(Client client : conseiller.getClients()) {
					if(idClient != null && idClient.equals(client.getId()) && client.isActif())
					return client;
				}
			}			
		}
		return null;
	}
	
	/**
	 * Recherche de l'agence dans la banque
	 * @param codeAgence
	 * @param banque
	 * @return une agence
	 */
	public static Agence rechercheAgence(String codeAgence, Bank banque) {
		for(Agence agence : banque.getListAgence()) {
			if(codeAgence != null && codeAgence.equals(agence.getCodeAgence()))
			{
				return agence;
			}
		}
		return null;
	}
	
	
	/**
	 * Connexion et verification du login du client
	 * @param loginClient
	 * @param passwordClient
	 * @param banque
	 * @return un client ou null si vertif incorrect
	 */
	public static Client seConnecter(String loginClient, String passwordClient, Bank banque) {
		for (Agence agence : banque.getListAgence()) {
			for(Conseiller conseiller : agence.getListConseiller()) {
				for(Client client : conseiller.getClients()) {
					if(loginClient != null && loginClient.equals(client.getLoginClient()) && passwordClient != null && passwordClient.equals(client.getPassword()) && client.isActif())
					return client;
				}
			}			
		}
		System.out.println("Login ou password incorrect");
		return null;
	}
	
	/**
	 * Connexion et verification du login de l'admin
	 * @param loginAdmin
	 * @param passwordAdmin
	 * @param banque
	 * @return un admin ou null si verif incorrect
	 */
	public static Admin seConnecterAdmin(String loginAdmin, String passwordAdmin, Bank banque) {
		Admin admin = banque.getAdmin();
		if(loginAdmin != null && loginAdmin.equals(admin.getLoginAdmin()) && passwordAdmin != null && passwordAdmin.equals(admin.getPasswordAdmin())){			
			return admin;				
		}
		System.out.println("Login ou password incorrect");
		return null;
	}
	
	/**
	 * Connexion et verification du login du conseiller
	 * @param loginConseiller
	 * @param passwordConseiller
	 * @param banque
	 * @return un conseiller ou null si verif incorrect
	 */
	public static Conseiller seConnecterConseiller(String loginConseiller, String passwordConseiller, Bank banque) {
		for (Agence agence : banque.getListAgence()) {
			for(Conseiller conseiller : agence.getListConseiller()) {
				if(loginConseiller != null && loginConseiller.equals(conseiller.getLoginConseiller()) && passwordConseiller != null && passwordConseiller.equals(conseiller.getPassWordConseiller())){			
					return conseiller;				
				}
			}
		}
		System.out.println("Login ou password incorrect");
		return null;
	}
	
	
}


