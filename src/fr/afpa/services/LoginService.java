package fr.afpa.services;
import java.util.Scanner;

import fr.afpa.*;
import fr.afpa.controles.Controls;
import fr.afpa.entite.*;

public class LoginService {
	
	/**
	 * Control du login client
	 * @return un client si le login correspond
	 */
	public static Client loginClient(Bank banque) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez votre login (10 chiffres) : ");
		String login_ = sc.nextLine();
		System.out.println("Entrez votre mdp : ");
		String mdp = sc.nextLine();
		Client client = BankServices.seConnecter(login_, mdp, banque);
		if(client != null  && client.isActif()) {
			return client;
		}
		return null;
	}
	
	/**
	 * Control du login conseiller
	 * @return un conseiller si le login correspond
	 */
	public static Conseiller loginConseiller(Bank banque) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez votre login (CO et 4 chiffres : ");
		String loginConseiller = sc.nextLine();
		System.out.println("Entrez votre mdp : ");
		String passwordConseiller = sc.nextLine();
		Conseiller conseiller = BankServices.seConnecterConseiller(loginConseiller, passwordConseiller, banque);
		if(conseiller != null) {
			return conseiller;
		}
		return null;
	}
	
	/**
	 * Control du login admin
	 * @return un admin si le login correspond
	 */
	public static Admin loginAdmin(Bank banque) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez votre login (ADM et 2 chiffres) : ");
		String loginAdmin = sc.nextLine();
		System.out.println("Entrez votre mdp : ");
		String passwordAdmin = sc.nextLine();
		Admin admin = BankServices.seConnecterAdmin(loginAdmin, passwordAdmin, banque);
		if(admin != null) {
			return admin;
		}
		return null;
	}
}
