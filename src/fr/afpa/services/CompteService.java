package fr.afpa.services;

import java.io.ObjectInputStream.GetField;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import fr.afpa.controles.Controls;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Bank;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.CompteLivretA;
import fr.afpa.entite.ComptePEL;
import fr.afpa.entite.Conseiller;

public class CompteService {
		
	
	/**
	 * rechercher un compte depuis la banque
	 * @param numCompte
	 * @param banque
	 * @return un compte
	 */
	public static Compte rechercheCompte(String numCompte, Bank banque) { 
		for (Agence agence : banque.getListAgence()) {
			for(Conseiller conseiller : agence.getListConseiller()) {
				for(Client client : conseiller.getClients()) {
					for(Compte compte : client.getListeCompteClient()) {
						if(numCompte != null && numCompte.equals(compte.getNumeroCompte())) {
							return compte;
						}
					}
				}
			}		
		}
		return null;
	}

	
	
	/**
	 * Service permettant de gerer le virement de deux comptes 
	 * @param compteAD compte a debiter
	 * @param compteAC compte a crediter
	 * @param montant
	 * @param banque
	 * @return vrai si le virement a ete effectuer sinon faux
	 */
	public static boolean virement(Compte compteAD, Compte compteAC, double montant) {
		if(Controls.controleSoldeValid(compteAD, montant)) {
			if(compteAD != null && compteAC != null) {
				compteAD.setSoldeCompte(compteAD.getSoldeCompte() - montant);
				//transaction fichier
				//date type(debit/ajouter) montant numero de compte
				compteAC.setSoldeCompte(compteAC.getSoldeCompte() + montant);
				//transaction fichier
				return true;
			}
		}
		return false;

	}
	
	
	/**
	 * Creer un compte
	 * @param banque
	 */
	public static void creationCompte (Bank banque) {
		
		Compte compte=null;
		Scanner sc = new Scanner(System.in);
		System.out.println("Quel type de compte souhaitez vous cr�er?");

		System.out.println("1-Compte Courant||2-Livret A||3-PEL");	
		int choix = sc.nextInt();
		sc.nextLine();
		
		System.out.println("Saisir votre identifiant client");
		String idClient1 = sc.nextLine();//verif ID CLIENT VALIDE
		Client client1=BankServices.rechercheClient(idClient1, banque);
		System.out.println("Saisir le num�ro du compte");
		String numeroCompte=sc.nextLine();//auto incr�ment�????
		System.out.println("Saisir code agence");
		String codeAgenceCompte=sc.nextLine();
		Agence agence=BankServices.rechercheAgence(codeAgenceCompte, banque);
		System.out.println("Saisir le montant du solde");
		double soldeCompte=sc.nextDouble();
		sc.nextLine();
		System.out.println("Decouvert autorise O/N");
		boolean decouvertCompte=false;
		String decouvert=sc.next();
		sc.nextLine();
		
		if ("O".equals(decouvert)) {
			decouvertCompte=true;
		}
		
		switch (choix){
		case 1:if (agence!=null) {
			compte = new CompteCourant(numeroCompte, codeAgenceCompte, soldeCompte, decouvertCompte);
				if (client1.getListeCompteClient()!= null && client1.getListeCompteClient().size()<3 ) {
					client1.getListeCompteClient().add(compte);
				}
		}
				break;
		
		case 2:
			if (agence!=null) {
			compte=new CompteLivretA(numeroCompte, codeAgenceCompte, soldeCompte, decouvertCompte);
			if (client1.getListeCompteClient()!= null && client1.getListeCompteClient().size()<3 ) {
			client1.getListeCompteClient().add(compte);
			break;
			}
			}
		case 3:if (agence!=null) {
			compte=new ComptePEL(numeroCompte, codeAgenceCompte, soldeCompte, decouvertCompte);
		if (client1.getListeCompteClient()!= null && client1.getListeCompteClient().size()<3 ) {
		client1.getListeCompteClient().add(compte);
			break;
		}
	}
}

		

	}
	
	/**
	 * Alimenter son compte
	 * @param compte
	 * @param montant
	 * @return R
	 */
	public static boolean alimenterCompte(Compte compte, double montant) {
		if(montant <= 0) {
			System.out.println("on peut pas ajouter une somme negative ou nulle");
			return false;
		}
		compte.setSoldeCompte(compte.getSoldeCompte() + montant);
		System.out.println("Votre compte est : "+ compte.getSoldeCompte() + " euros");
		String chemin = "C:\\ENV\\Ressources\\transaction.txt";
		String credit = "Credit";
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
		String date = localDate.format(formatter);
		String montantStr = ""+montant;
		if(Operation.transaction(chemin, compte, montantStr, date, credit)) {
			System.out.println("transaction reussi!");
		}
		else {
			System.out.println("Impossible d'ecrire dans le fichier");
		}
		return true;
	}
	
	
	/**
	 * Retire de l'argent avec la transaction dans un fichier
	 * @param compte
	 * @param montant
	 * @return vrai si la transaction se passe bien 
	 */
	public static boolean retraitArgent(Compte compte, double montant) {
		if(montant <= 0 ) {
			System.out.println("on peut pas retirer une somme negative ou nulle");
			return false;
		}
		if(Controls.controleSoldeValid(compte, montant)) {
			compte.setSoldeCompte(compte.getSoldeCompte() - montant);
			System.out.println("Votre compte est : "+ compte.getSoldeCompte() + " euros");
			String chemin = "C:\\ENV\\Ressources\\transaction.txt";
			String credit = "Debit";
			LocalDate localDate = LocalDate.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
			String date = localDate.format(formatter);
			String montantStr = "-"+montant;
			if(Operation.transaction(chemin, compte, montantStr, date, credit)) {
				System.out.println("transaction reussi!");
			}
			else {
				System.out.println("Impossible d'ecrire dans le fichier");
			}
		}
		return true;
	}

	
	/**
	 * Affichage du smiley selon le solde du compte
	 * @param compte
	 * @return un smiley :)
	 */
	public static String smiley(Compte compte) {
		if(compte.getSoldeCompte() > 0) {
			return ":)";
		}
		else if(compte.getSoldeCompte() < 0) {
			return ":(";
		}
		else {
			return ":|";
		}
	}
		//auto incrementation num compte
		public static void numCompteGenerator(Compte compte) {
			compte.getNumeroCompte();
			
	}
}

		

		


