package fr.afpa.services;

import java.util.LinkedList;
import java.util.Scanner;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

import fr.afpa.controles.Controls;
import fr.afpa.entite.*;


public class AgenceServices {
	
	/**
	 * creer une agence
	 * @return une agence
	 */
	public Agence createAgence() {
		String codeAgence;
		Scanner sc = new Scanner(System.in);
		System.out.println("Saisir nom agence :");
		String nomAgence = sc.nextLine();
		do {
			System.out.println("Saisir code Agence (3 chiffres):");
			codeAgence = sc.nextLine();
		}while(!Controls.controlCodeAgence(codeAgence));
		
		System.out.println("Saisir adresse de l'agence");
		String adresseAgence = sc.nextLine();
		Agence agence  = null;
	    agence = new Agence(codeAgence, nomAgence, adresseAgence);		
		return agence;
	}
	
	/**
	 * Creation d'agence (suite)
	 * @param agence
	 */
	public static void createConseiller(Agence agence) {
		Conseiller conseiller = new ConseillerServices().createConseiller(agence);
		if(conseiller != null) {
			agence.getListConseiller().add(conseiller);
		}
	}
	
	/**
	 * recupere un conseiller (methode rate mais fonctionnelle)
	 * @param banque
	 * @return un conseiller
	 */
	public static Conseiller getConseiller(Bank banque) {
		Agence agence = BankServices.getAgence(banque);
		Scanner in = new Scanner(System.in);
		if(agence != null) {
			System.out.println("Entrez le nom de votre conseiller : ");
			String nomConseiller = in.nextLine();
			for (Conseiller conseiller : agence.getListConseiller()) {
				if(nomConseiller.equals(conseiller.getNom())) {
					return conseiller;
				}
			}
		}
		System.out.println("Code agence incorrect ou creer une agence");
		return null;
	}
	
		/**
		 * Rechercher un compte depuis la liste des conseillers de l'agence
		 * @param numCompte
		 * @param agence
		 * @return un compte
		 */
		public static Compte rechercheCompte(String numCompte, Agence agence) { 	
			for(Conseiller conseiller : agence.getListConseiller()) {
				for(Client client : conseiller.getClients()) {
					for(Compte compte : client.getListeCompteClient()) {
						if(numCompte != null && numCompte.equals(compte.getNumeroCompte())) {
							return compte;
						}
					}
				}
			}
			return null;
		}
	
}
