package fr.afpa.services;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDate;

import fr.afpa.entite.*;

public class Operation {
	
	/**
	 * function permettant d'ecrire dans un fichier
	 * @param id
	 * @param login
	 * @param password
	 * @param chemin
	 */
	public static boolean transaction(String chemin,Compte compte, String montant, String date, String debitOrCredit) {
		try {
			FileWriter fw = new FileWriter(chemin, true);
			BufferedWriter bw = new BufferedWriter(fw);		
			bw.write(date);
			bw.write(";");
			bw.write(debitOrCredit);
			bw.write(";");
			bw.write(montant);
			bw.write(";");
			bw.write(compte.getNumeroCompte());
			bw.newLine();
			bw.close();
		}
		catch(Exception e) {
			System.out.println("Erreur "+e);
			return false;
		}
		return true;
	}
	
	/**
	 * cree un fichier contenant l'id du client, login et password en precisant le chemin du fichier
	 * @param id
	 * @param login
	 * @param password
	 * @param chemin
	 */
	public static void createFichier(String id, String login, String password, String chemin) {
		try {
			FileWriter fw = new FileWriter(chemin, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(id);
			bw.write(";");
			bw.write(login);
			bw.write(";");
			bw.write(password);
			bw.newLine();	
			bw.close();
		}
		catch(Exception e) {
			System.out.println("Erreur "+e);
		}
	}
	
	/**
	 * lire fichier du client
	 * @return un tableau contenant toutes les lignes
	 */
	public static String[] lireFichier(String chemin) {
		String[] strFichier=null;
		try {
			FileReader fr = new FileReader(chemin);
			BufferedReader br = new BufferedReader(fr);
			int i=0;
			while(br.ready()) {
				strFichier[i] += br.readLine();
				i++;
			}
			br.close();
		}
		catch(Exception e) {
			System.out.println("Erreur "+e);
		}
		
		return strFichier;
	}
	
	/**
	 * Parcourir le fichier pour trouver le login du client
	 * @param login
	 * @return un indice
	 */
	public static int parcourirFichierLogin(String login, String chemin) {
		String[] strFichier = lireFichier(chemin);
		int indice=0;
		for(int i=0; i<strFichier.length;i++) {
			String ligneActuelle = lireLigneFichier(i,chemin);
			String[] infosActuelle = getInfosLigneFichier(ligneActuelle);
			if(login.equals(infosActuelle[1])) {
				indice = i;
				break;
			}		
		}
		return indice;
		
	}
	
	/**
	 * Lire une ligne pr�cise du fichier
	 * @param indice
	 * @return une ligne
	 */
	public static String lireLigneFichier(int indice, String chemin) {
		String[] strFichier = lireFichier(chemin);
		String ligneFichier = strFichier[indice];
		return ligneFichier;
	}
	
	/**
	 * Retourne un tableau contenant les infos d'un client
	 * @param ligneFichier
	 * @return
	 */
	public static String[] getInfosLigneFichier(String ligneFichier) {
		String[] infos = ligneFichier.split(";");
		return infos;
	}
	

	
}
